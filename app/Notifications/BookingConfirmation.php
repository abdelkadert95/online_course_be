<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\URL;


class BookingConfirmation extends Notification{
    use Queueable;

    protected $booking;

    public function __construct($booking){
        $this->booking = $booking;
    }

    public function via($notifiable){
        return ['mail'];
    }

    public function toMail($notifiable){
        $bookingUrl = 'http://127.0.0.1:5500/confirm.html?booking_id=' . $this->booking->id;
    
        return (new MailMessage)
            ->subject('Confirmation de réservation')
            ->line('Veuillez confirmer votre réservation pour le cours ' . $this->booking->course->titre . ' en cliquant sur le bouton ci-dessous.')
            ->line('Date de réservation : ' . $this->booking->date_de_reservation)
            ->action('Confirmer la réservation', $bookingUrl)
            ->line('Merci d\'utiliser notre application!');
    }

    public function toArray($notifiable){
        return [
            //
        ];
    }
}
