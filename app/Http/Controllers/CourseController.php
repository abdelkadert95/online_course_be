<?php

namespace App\Http\Controllers;

use App\Models\Course;
use App\Models\Courses;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CourseController extends Controller{
    //
    public function create_course(Request $request){
        $validate = Validator::make($request->all(), [
            'titre' => 'required|string|max:250',
            'description' => 'nullable|string',
            'date' => 'required|date',
            'heure' => 'required|date_format:H:i:s',
            'durée' => 'required|integer',
        ]);

        if ($validate->fails()) {
            return response()->json([
                'status' => 'failed',
                'message' => 'Validation Error!',
                'data' => $validate->errors(),
            ], 403);
        }

        $course = Course::create($request->all());

        return response()->json([
            'status' => 'success',
            'message' => 'Course created successfully.',
            'data' => $course,
        ], 201);
    }

    public function  all_courses(){
        $users = Course::all();
        return response()->json([
            'status' => 'success',
            'message' => 'All courses retrieved successfully.',
            'data' => $users,
        ], 200);
    }

    public function signle_course($id){
        $course = Course::find($id);
    
        if (!$course) {
            return response()->json(['error' => 'Course not found'], 404);
        }
        return response()->json(['data' => $course], 200);
    }
}
