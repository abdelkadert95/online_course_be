<?php

namespace App\Http\Controllers;

use App\Models\Booking;
use App\Models\User;
use App\Notifications\BookingConfirmation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class BookingController extends Controller{
    //
    public function create_booking(Request $request) {
        // Validation
        $validator = Validator::make($request->all(), [
            'user_id' => 'required|exists:users,id',
            'course_id' => 'required|exists:courses,id',
            'date_de_reservation' => 'required|date'
        ]);
    
        // Handle validation failures
        if ($validator->fails()) {
            return response()->json([
                'status' => 'failed',
                'message' => 'Validation Error!',
                'data' => $validator->errors(),
            ], 403);
        }
    
        // Check for existing booking for the same user and course at the same date
        $existingBooking = Booking::where('user_id', $request->user_id)
            ->where('course_id', $request->course_id)
            ->where('date_de_reservation', $request->date_de_reservation)
            ->first();
    
        if ($existingBooking) {
            return response()->json([
                'status' => 'failed',
                'message' => 'You already have a booking for this course at the specified time.',
            ], 409); // Conflict status code
        }
    
        // Create new booking
        $booking = Booking::create([
            'user_id' => $request->user_id,
            'course_id' => $request->course_id,
            'date_de_reservation' => $request->date_de_reservation,
        ]);
    
        // Fetch the user to send the notification
        $user = User::find($request->user_id);
    
        // Send booking confirmation notification
        $user->notify(new BookingConfirmation($booking));
    
        // Return successful response
        return response()->json([
            'status' => 'success',
            'message' => 'Booking created successfully.',
            'data' => $booking,
        ], 201);
    }

    public function confirmBooking(Booking $booking){
        // Update booking status
        $booking->update(['status' => 'confirmed']);
    
        // Optionally, perform any other actions related to confirmation
    
        return response()->json([
            'status' => 'success',
            'message' => 'Booking confirmed successfully.',
            'data' => $booking,
        ]);
    }
    
    
    public function show_booking($user_id){
        // Retrieve bookings for the specified user
        $bookings = Booking::where('user_id', $user_id)->get();
    
        if ($bookings->isEmpty()) {
            return response()->json([
                'status' => 'failed',
                'message' => 'No bookings found for this user.',
            ], 404);
        }
    
        return response()->json([
            'status' => 'success',
            'message' => 'Bookings retrieved successfully.',
            'data' => $bookings,
        ], 200);
    }

}
