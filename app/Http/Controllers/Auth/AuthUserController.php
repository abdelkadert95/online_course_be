<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class AuthUserController extends Controller{
    //
    public function create_user(Request $request){
        $validate = Validator::make($request->all(), [
            'nom' => 'required|string|max:250',
            'prénom' => 'required|string|max:250',
            'email' => 'required|string|email:rfc,dns|max:250|unique:users,email',
            'password' => 'required|string|min:8|confirmed'
        ]);

        if($validate->fails()){
            return response()->json([
                'status' => 'failed',
                'message' => 'Validation Error!',
                'data' => $validate->errors(),
            ], 403);
        }

        $user = User::create([
            'nom' => $request->nom,
            'prénom' => $request->prénom,
            'email' => $request->email,
            'password' => Hash::make($request->password)
        ]);

        // $data['token'] = $user->createToken($request->email)->accessToken;
        $data['user'] = $user;

        $response = [
            'status' => 'success',
            'message' => 'User is created successfully.',
            'data' => $data,
        ];

        return response()->json($response, 201);
    }

    public function login(Request $request){
        $validate = Validator::make($request->all(), [
            'email' => 'required|string|email',
            'password' => 'required|string'
        ]);

        if($validate->fails()){
            return response()->json([
                'status' => 'failed',
                'message' => 'Validation Error!',
                'data' => $validate->errors(),
            ], 403);  
        }

        // Check email exist
        $user = User::where('email', $request->email)->first();

        // Check password
        if(!$user || !Hash::check($request->password, $user->password)) {
            return response()->json([
                'status' => 'failed',
                'message' => 'Invalid credentials'
                ], 401);
        }

        $data['token'] = $user->createToken($request->email)->accessToken;
        $data['user'] = $user;
        
        $response = [
            'status' => 'success',
            'message' => 'User is logged in successfully.',
            'data' => $data,
        ];

        return response()->json($response, 200);
    } 

   
    public function logout(Request $request) {
        $guard = Auth::getDefaultDriver();
    
        if ($guard === 'api' && Auth::guard('api')->check()) {
            $user = Auth::guard('api')->user();
        } else {
            return response()->json([
                'status' => 'error',
                'message' => 'User not authenticated'
            ], 401);
        }
    
        if ($user) {
            // Delete the current token
            $request->user()->token()->delete();
    
            return response()->json([
                'status' => 'success',
                'message' => 'User is logged out successfully'
            ], 200);
        } else {
            return response()->json([
                'status' => 'error',
                'message' => 'User not authenticated'
            ], 401);
        }
    }

    public function all_users(){
        $users = User::all();

        return response()->json([
            'status' => 'success',
            'message' => 'All users retrieved successfully.',
            'data' => $users,
        ], 200);
    }
}
