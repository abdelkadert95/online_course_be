<?php

use App\Http\Controllers\Auth\AuthUserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\Auth\LoginRegisterController;
use App\Http\Controllers\BookingController;
use App\Http\Controllers\CourseController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

// Public routes of authtication
Route::controller(AuthUserController::class)->group(function() {
    Route::post('/users', 'create_user');
    Route::post('/login_user', 'login');
    Route::get('/users', 'all_users');
    
});

// // Protected routes of product and logout
Route::middleware('auth:api')->group( function () {
    Route::post('/logout_user', [AuthUserController::class, 'logout']);
    Route::post('/bookings', [BookingController::class, 'create_booking']);
    Route::get('/bookings/{user_id}', [BookingController::class, 'show_booking']);
    Route::get('/courses', [CourseController::class, 'all_courses']);
    Route::get('/course/{id}', [CourseController::class, 'signle_course']);
    Route::patch('/bookings/{booking}/confirm', [BookingController::class, 'confirmBooking']);
});


// Public routes of course
Route::controller(CourseController::class)->group(function() {
    Route::post('/courses', 'create_course');

});